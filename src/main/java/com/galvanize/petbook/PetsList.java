package com.galvanize.petbook;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class PetsList {
    private ArrayList<Pet> petsList;

    public ArrayList<Pet> getPetsList() {
        return petsList;
    }

    public void setPetsList(ArrayList<Pet> petsList) {
        this.petsList = petsList;
    }

    public PetsList() {
        this.petsList = new ArrayList<>();
    }

    public PetsList(ArrayList<Pet> petsList){
        this.petsList = petsList;
    }

    public PetsList(List<Pet> petsList){
        this.petsList = new ArrayList<Pet>(petsList);
    }

    public void addPet(Pet p) {
        petsList.add(p);
    }

    public boolean isEmpty() {
        return petsList == null || petsList.isEmpty();
    }

    public int getCount() {
        return petsList.size();
    }
}
