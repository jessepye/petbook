package com.galvanize.petbook;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PetsService {

    PetsRepository petsRepository;

    public PetsService(PetsRepository petsRepository) {
        this.petsRepository = petsRepository;
    }

    public PetsList getPets() {
        List<Pet> pets = petsRepository.findAll();
        if (!pets.isEmpty()) {
            return new PetsList(pets);
        } else {
            return new PetsList();
        }
    }

    public Pet addPet(Pet newPet) {
        return petsRepository.save(newPet);
    }
}
