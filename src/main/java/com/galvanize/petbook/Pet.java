package com.galvanize.petbook;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = "pets")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long databaseId;
    @Column(unique = true)
    private int id;
    private String name;
    private String photoUrl;
    private double age;
    private String species;

    public Pet() {

    }

    public Pet(int id, String name, String photoUrl) {
        this.id = id;
        this.name = name;
        this.photoUrl = photoUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

}
