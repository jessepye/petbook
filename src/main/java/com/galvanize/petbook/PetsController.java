package com.galvanize.petbook;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/api/pets")
public class PetsController {
    PetsService petsService;

//    public PetsController() {
//        petsService = new PetsService();
//    }

    public PetsController(PetsService petsService) {
        this.petsService = petsService;
    }

    @GetMapping
    public ResponseEntity<PetsList> getPets() {
        PetsList result = petsService.getPets();
        return ResponseEntity.ok(result);
    }

    @PostMapping
    public Pet addPet(@RequestBody Pet newPet) {
        return petsService.addPet(newPet);
    }

}
