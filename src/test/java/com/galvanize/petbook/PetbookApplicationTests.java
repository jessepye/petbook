package com.galvanize.petbook;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@TestPropertySource(locations= "classpath:application-test.properties")

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PetbookApplicationTests {
	@Autowired
	TestRestTemplate testRestTemplate;

	@Autowired
	PetsRepository petsRepository;

	List<Pet> defaultPetsList;

	@BeforeEach
	void setUp() {
		defaultPetsList = new ArrayList<>();
		defaultPetsList.add(new Pet(0, "Spot", "example.com/0.jpg"));
		defaultPetsList.add(new Pet(1, "Luna", "example.com/1.jpg"));
		defaultPetsList.add(new Pet(2, "Bella", "example.com/2.jpg"));
		defaultPetsList.add(new Pet(3, "Oliver", "example.com/3.jpg"));
		defaultPetsList.add(new Pet(4, "Charlie", "example.com/4.jpg"));
		petsRepository.saveAll(defaultPetsList);
	}

//	@Test
//	void contextLoads() {
//	}

	@Test
	void getPets_noArgs_exists_returnsPetsList() {
		ResponseEntity<PetsList> response = testRestTemplate.getForEntity("/api/pets", PetsList.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isNotNull();
		assertThat(response.getBody().isEmpty()).isFalse();
	}

}
