package com.galvanize.petbook;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class PetsServiceTests {
    PetsService petsService;

    @Mock
    PetsRepository petsRepository;

    Pet defaultPet;

    @BeforeEach
    void setUp() {
        petsService = new PetsService(petsRepository);
        defaultPet = new Pet(0, "Spot", "example.com/0.jpg");
    }

    @Test
    void getPets_hasContent_returnsList(){
        when(petsRepository.findAll()).thenReturn(Arrays.asList(defaultPet));

        PetsList petsList = petsService.getPets();

        assertThat(petsList).isNotNull();
        assertThat(petsList.isEmpty()).isFalse();
        assertThat(petsList.getCount()).isEqualTo(1);
    }

    @Test
    void addPet_valid_returnsPet(){
        when(petsRepository.save(any(Pet.class))).thenReturn(defaultPet);

        Pet savedPet = petsService.addPet(defaultPet);

        assertThat(savedPet).isNotNull();
        assertThat(savedPet.getId()).isEqualTo(defaultPet.getId());

    }
}
