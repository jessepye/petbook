package com.galvanize.petbook;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("unused")
@WebMvcTest(PetsController.class)
public class PetsControllerTests {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    PetsService petsService;

    ObjectMapper objectMapper;

    ArrayList<Pet> defaultPetsList;
    Pet defaultPet;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();

        defaultPet = new Pet(0, "Spot", "example.com/0.jpg");

        defaultPetsList = new ArrayList<>();
        defaultPetsList.add(new Pet(0, "Spot", "example.com/0.jpg"));
        defaultPetsList.add(new Pet(1, "Luna", "example.com/1.jpg"));
        defaultPetsList.add(new Pet(2, "Bella", "example.com/2.jpg"));
        defaultPetsList.add(new Pet(3, "Oliver", "example.com/3.jpg"));
        defaultPetsList.add(new Pet(4, "Charlie", "example.com/4.jpg"));
    }

    @Test
    void getPets_noArgs_exists_returnsPetsList() throws Exception {
        when(petsService.getPets()).thenReturn(new PetsList(defaultPetsList));

        mockMvc.perform(get("/api/pets"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.petsList", hasSize(5)));
    }

    @Test
    void postPet_valid_returnsAuto() throws Exception {
        when(petsService.addPet(any(Pet.class))).thenReturn(defaultPet);

        mockMvc.perform(post("/api/pets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(defaultPet)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Spot"));
    }
}
